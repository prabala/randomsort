package com.example.demo;

import static org.mockito.Mockito.when;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.validation.BindingResult;
import com.example.demo.controller.RandomSortController;
import com.example.demo.domain.RandomSortDo;
import com.example.demo.domain.RandomSortResponse;
import com.example.demo.service.RandomSortService;

@RunWith(MockitoJUnitRunner.class)
public class RandomSortControllerTest {

	@Autowired
	RandomSortService randomSortService;

	@InjectMocks
	RandomSortController randomSortController = new RandomSortController();

	@Mock
	HttpServletRequest request;

	@Mock
	HttpServletResponse response;

	@Mock
	BindingResult bindingResult;

	@Test
	public void testRandomSort() {
		try {
			RandomSortDo randomSortDo = new RandomSortDo();
			RandomSortResponse randomSortResponse = new RandomSortResponse();
			randomSortDo.setNoOfElements(6);
			when(randomSortService.doRandomSorting(randomSortDo)).thenReturn(randomSortResponse);
			randomSortDo.setNoOfElements(10);
			when(randomSortService.doRandomSorting(randomSortDo)).thenReturn(randomSortResponse);
		} catch (Exception e) {
			System.out.println("Exception : " + e);
		}
	}
}