package com.example.demo.po;

import javax.validation.Valid;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Pattern;

public class RandomSort {
		
	@Valid
	@NotEmpty(message = "No Of Elements should not be Empty") 	
	private int noOfElements;
		
    public int getNoOfElements() {
        return noOfElements;
    }

    public void setNoOfElements(int noOfElements) {
        this.noOfElements = noOfElements;
    }

}
