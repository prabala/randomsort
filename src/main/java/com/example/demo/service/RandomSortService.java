package com.example.demo.service;

import java.util.Arrays;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import com.example.demo.domain.RandomSortDo;
import com.example.demo.domain.RandomSortResponse;
import com.example.demo.entity.RandomSortEntity;
import com.example.demo.repository.RandomSortRepository;

@Service
public class RandomSortService {

	@Autowired
	RandomSortRepository randomSortRepository;

	@Autowired
	RandomSortResponse randomSortResponse;

	public RandomSortResponse doRandomSorting(RandomSortDo randomSortDo) throws Exception {
		Long startTime = System.currentTimeMillis();
		double[] originalRandomNumbers = generateRandomNumber(randomSortDo);
		String originalRandoms = Arrays.toString(originalRandomNumbers);

		double[] sortedNumber = mergeSort(originalRandomNumbers, 0, originalRandomNumbers.length - 1);

		Long elapsedTime = System.currentTimeMillis() - startTime;
		try {
			RandomSortEntity randomSort = new RandomSortEntity(originalRandomNumbers.length,
					Arrays.toString(originalRandomNumbers), Arrays.toString(sortedNumber));
			randomSortRepository.save(randomSort);
		} catch (Exception e) {
			System.out.println("DB exception : " + e);
		}

		randomSortResponse.setOriginalNumbers(originalRandoms);
		randomSortResponse.setElapsedTime(elapsedTime.toString());
		randomSortResponse.setNoOfElements(Integer.toString(randomSortDo.getNoOfElements()));

		randomSortResponse.setSortedNumbers(Arrays.toString(sortedNumber));

		return randomSortResponse;
	}

	public double[] generateRandomNumber(RandomSortDo randomSortDo) {
		int totalNumbers = randomSortDo.getNoOfElements();
		double[] randomNumbers = new double[totalNumbers];
		for (int i = 0; i < totalNumbers; i++)
			randomNumbers[i] = Math.random();
		return randomNumbers;
	}

	public static double[] mergeSort(double[] elements, int low, int high) {
		double[] sortedNumbers = new double[high];
		if (low < high) {
			int mid = (low + high) / 2;
			mergeSort(elements, low, mid);
			mergeSort(elements, mid + 1, high);
			sortedNumbers = merge(elements, low, mid, high);
		}
		return sortedNumbers;
	}

	private static double[] merge(double[] subset, int low, int mid, int high) {
		int n = high - low + 1;
		double[] Temp = new double[n];

		int i = low, j = mid + 1;
		int k = 0;

		while (i <= mid || j <= high) {
			if (i > mid)
				Temp[k++] = subset[j++];
			else if (j > high)
				Temp[k++] = subset[i++];
			else if (subset[i] < subset[j])
				Temp[k++] = subset[i++];
			else
				Temp[k++] = subset[j++];
		}
		for (j = 0; j < n; j++)
			subset[low + j] = Temp[j];
		return subset;
	}

}
