package com.example.demo.controller;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.domain.RandomSortDo;
import com.example.demo.domain.RandomSortResponse;
import com.example.demo.service.RandomSortService;

@RestController
public class RandomSortController {

	@Autowired
	RandomSortService randomSortService;

	@RequestMapping(value = "/randomsort", method = RequestMethod.POST, consumes = "application/json", produces = {
			"application/json;charset=UTF-8" })
	public ResponseEntity<byte[]> randomSort(@Valid @RequestBody com.example.demo.po.RandomSort randomSortPo,
			BindingResult result, HttpServletRequest request) {
		HttpHeaders responseHeaders = new HttpHeaders();
		responseHeaders.setContentType(MediaType.APPLICATION_JSON);

		ResponseEntity<byte[]> responseBytes = null;

		if (result.hasErrors()) {

			return new ResponseEntity<>(result.toString().getBytes(), responseHeaders, HttpStatus.OK);

		} else {

			try {
				RandomSortDo randomSortDo = new RandomSortDo();
				randomSortDo.setNoOfElements(randomSortPo.getNoOfElements());
				RandomSortResponse response = randomSortService.doRandomSorting(randomSortDo);
				return new ResponseEntity<>(response.toString().getBytes(), responseHeaders, HttpStatus.OK);
			} catch (Exception e) {
				byte[] excep = e.toString().getBytes();
				return new ResponseEntity<>(excep, responseHeaders, HttpStatus.OK);
			}
		}
	}

}